using UnityEngine;
using TMPro;

public class KeyboardMenager : MonoBehaviour
{
    public static KeyboardMenager Instance;
    [SerializeField] TextMeshProUGUI textBox;
    [SerializeField] TextMeshProUGUI printBox;
    [HideInInspector] public IAudioPlayer audioPlayer;

    private void Start()
    {
        Instance = this;
        printBox.text = "";
        textBox.text = "";
    }
    public void PlayClickSound()
    {
        audioPlayer.PlaySound("Click");
    }
    public void DeleteLetter()
    {
        PlayClickSound();
        if (textBox.text.Length != 0) {
            textBox.text = textBox.text.Remove(textBox.text.Length - 1, 1);
        }
    }

    public void AddLetter(string letter)
    {
        PlayClickSound();
        textBox.text = textBox.text + letter;
    }
    private string _target;
    public void SetTarget(string target)
    {
        _target = target;
    }
    public void SubmitWord()
    {
        PlayClickSound();
        printBox.text = textBox.text;
        if(_target!=null && printBox.text!="")
        {
            PlayerPrefs.SetString(_target, printBox.text);
        }
        textBox.text = "";
        this.GetComponentInParent<LobbyCanvas>().SwitchView(1);
        // Debug.Log("Text submitted successfully!");
    }
}
