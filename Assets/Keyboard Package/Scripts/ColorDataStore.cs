using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorDataStore : MonoBehaviour
{
    [SerializeField] Color borderColor;
    [SerializeField] Color fillColor;
    [SerializeField] Color textColor;
    [SerializeField] Color actionTextColor;

    [SerializeField] Color fillSelColor= Color.yellow;
    [SerializeField] Color textSelColor= Color.magenta;
    [SerializeField] Color actionTextSelColor= Color.blue;

    static Color _borderColor;
    static Color _fillColor;
    static Color _textColor;
    static Color _actionTextColor;

    static Color _fillSelColor;
    static Color _textSelColor;
    static Color _actionTextSelColor;

    public static Color FillSelColor { get => _fillSelColor; }
    public static Color TextSelColor { get => _textSelColor; }
    public static Color ActionTextSelColor { get => _actionTextSelColor;}

    private void Awake()
    {
        _borderColor = borderColor;
        _fillColor = fillColor;
        _textColor = textColor;
        _actionTextColor = actionTextColor;
        _fillSelColor = fillSelColor;
        _textSelColor = textSelColor;
        _actionTextSelColor = actionTextSelColor;
    }

    public static Color GetKeyboardBorderColor() { return _borderColor; }
    public static Color GetKeyboardFillColor() { return _fillColor; }
    public static Color GetKeyboardTextColor() { return _textColor; }
    public static Color GetKeyboardActionTextColor() { return _actionTextColor; }
}
