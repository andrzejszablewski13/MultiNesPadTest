using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class KeyboardButtonController : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    [SerializeField] Image containerBorderImage;
    [SerializeField] Image containerFillImage;
    [SerializeField] Image containerIcon;
    [SerializeField] TextMeshProUGUI containerText;
    [SerializeField] TextMeshProUGUI containerActionText;
    private Button _buton;
    [SerializeField] private bool _select = false;


    private void Start() {
        SetContainerBorderColor(ColorDataStore.GetKeyboardBorderColor());
        SetContainerFillColor(ColorDataStore.GetKeyboardFillColor());
        SetContainerTextColor(ColorDataStore.GetKeyboardTextColor());
        SetContainerActionTextColor(ColorDataStore.GetKeyboardActionTextColor());
        _buton = this.GetComponent<Button>();
        if (_select)
        {
            _buton.Select();
            ColorSelect();
        }
    }

    public void SetContainerBorderColor(Color color) => containerBorderImage.color = color;
    public void SetContainerFillColor(Color color) => containerFillImage.color = color;
    public void SetContainerTextColor(Color color) => containerText.color = color;
    public void SetContainerActionTextColor(Color color) { 
        containerActionText.color = color;
        containerIcon.color = color;
    }

    public void AddLetter() {
        if(KeyboardMenager.Instance != null) {
            KeyboardMenager.Instance.AddLetter(containerText.text);
        } else {
            Debug.Log(containerText.text + " is pressed");
        }
    }
    public void DeleteLetter() { 
        if(KeyboardMenager.Instance != null) {
            KeyboardMenager.Instance.DeleteLetter();
        } else {
            Debug.Log("Last char deleted");
        }
    }
    public void SubmitWord() {
        if(KeyboardMenager.Instance != null) {
            KeyboardMenager.Instance.SubmitWord();
        } else {
            Debug.Log("Submitted successfully!");
        }
    }

    public void OnSelect(BaseEventData eventData = null)
    {
        ColorSelect();
    }

    public void OnDeselect(BaseEventData eventData=null)
    {
        ColorDeSelect();
    }
    private void OnDisable()
    {
        ColorDeSelect();
    }
    public void ColorSelect()
    {
        SetContainerFillColor(ColorDataStore.FillSelColor);
        SetContainerTextColor(ColorDataStore.TextSelColor);
        SetContainerActionTextColor(ColorDataStore.ActionTextSelColor);
        _select = true;
    }
    public void ColorDeSelect()
    {
        SetContainerFillColor(ColorDataStore.GetKeyboardFillColor());
        SetContainerTextColor(ColorDataStore.GetKeyboardTextColor());
        SetContainerActionTextColor(ColorDataStore.GetKeyboardActionTextColor());
        _select = false;
    }
}