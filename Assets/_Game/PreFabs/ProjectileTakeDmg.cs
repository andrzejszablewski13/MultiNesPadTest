using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
public class ProjectileTakeDmg : MonoBehaviour,ITakeDMG
{
    private IHadHP _player;
    private void Awake()
    {
        _player = this.GetComponentInParent<Projectile>();
    }
    public void HIt(PlayerRef player,
        int value = 1, Vector3 hitpoint = default(Vector3), float knockbackpower = 0)
    {
        _player.Hp -= value;
        _player.lastattackedplayer = player;

    }
}
