using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class AudioPrefab : MonoBehaviour
{
    private AudioSource _audio;
    private SoundZenMan.Sound _sound;
    private bool _soundCountLowered, _destroyed;
    private Pool _pool;
    private IEnumerator ClearAfterSound()
    {
        float delay = _sound.clips[0].length * 0.7f+1;
        yield return new WaitForSeconds(delay);
        _sound.simultaneousPlayCount--;
        _soundCountLowered = true;
        if (!_audio.loop)
        {
            yield return new WaitForSeconds(_sound.clips[0].length - delay+1);
            _destroyed = true;
            _pool.Despawn(this);
        }
        
    }
    public class Pool : MonoMemoryPool<SoundZenMan.Sound, Pool, AudioPrefab>
    {
        protected override void OnCreated(AudioPrefab item)
        {
            base.OnCreated(item);
            item._audio = item.GetComponent<AudioSource>();
        }
        protected override void Reinitialize(SoundZenMan.Sound sound, Pool p1, AudioPrefab item)
        {
            base.Reinitialize(sound,p1, item);
            item._soundCountLowered = false;
            item._destroyed = false;
            item._sound = sound;
            item._pool = p1;
            item.StartCoroutine(item.ClearAfterSound());
        }
        protected override void OnDespawned(AudioPrefab item)
        {
            base.OnDespawned(item);
            item.StopAllCoroutines();
            if (!item._soundCountLowered)
            {
                item._sound.simultaneousPlayCount--;
                item._soundCountLowered = true;
            }
            if (!item._destroyed)
            {
                item._destroyed = true;
                item._pool.Despawn(item);
            }
        }
        protected override void OnDestroyed(AudioPrefab item)
        {
            base.OnDestroyed(item);
            item.StopAllCoroutines();
            if (!item._soundCountLowered)
            {
                item._sound.simultaneousPlayCount--;
                item._soundCountLowered = true;
            }
            if (!item._destroyed)
            {
                item._destroyed = true;
                item._pool.Despawn(item);
            }
        }
    }
}
