﻿/// <summary>
/// interface to toggle audio and music off/on
/// </summary>
public interface IToggleAudio
{
    public void ToggleMusic();
    public void ToggleMute();
    public bool IsMusicOff();
    public bool IsMuted();
}
