﻿using UnityEngine;
/// <summary>
/// interface to play audio 
/// </summary>
public interface IAudioPlayer
{
    public AudioSource PlaySound(string key, bool autoScaleVolume = true, float maxVolumeScale = 1f);
    public AudioSource PlaySound(string key, Vector3 pos, bool autoScaleVolume = true, float maxVolumeScale = 1f);

    public AudioSource PlayMusic(string evRef, Vector3 pos = default(Vector3),bool loop=true);
}

