using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Zenject;
[CreateAssetMenu(fileName ="AudioController",menuName ="AudioContr")]
    public class SoundZenMan : ScriptableObject, IAudioPlayer, IToggleAudio, IAudioVolume
{
        [SerializeField] [Range(0f, 1f)] private float _globalVolume = 1f;
    [SerializeField] [Range(0f, 1f)] private float _musicVolume = 1f;
    [SerializeField] [Range(0f, 1f)] private float _soundVolume = 1f;
    [System.Serializable]
    //class that contains all sound ref
        public class Sound
        {
            [Tooltip("key to activate sound ")]
            public string key;//key to activate sound
            [Tooltip("max volume of sound")]
            [Range(0f, 1f)] public float soundVolume = 1;//max volume of sound
            [Tooltip("clips - from them one is randomly selected to be played")]
            public AudioClip[] clips;//clips - from them one is randomly selected to be played
            [HideInInspector]
            public int simultaneousPlayCount = 0;//track of number actual playing sounds of this object
        }

        [Header("Max number allowed of same sounds playing together")]
        public int maxSimultaneousSounds = 7;
        [Tooltip("Update this list in runtime dont work. Script on start create a dictionary from this list. Keays are values names key in Sound object ")]
        public List<Sound> _sounds = new List<Sound>();
        private Dictionary<string, Sound> _soundDictionary;

        private AudioSource _musicSource;
        private const string MUTE_PREF_KEY = "MutePreference";
        private const int MUTED = 1;
        private const int UN_MUTED = 0;
        private const string MUSIC_PREF_KEY = "MusicPreference";
        private const int MUSIC_OFF = 0;
        private const int MUSIC_ON = 1;
    private string _usedMusicKey;
    //refs for IAudioVolume
    public float MusicVolume { get => _musicVolume; set { _musicVolume = Mathf.Clamp01(value); UpdateMusicVol(); } }
    public float SoundVolume { get => _soundVolume; set { _soundVolume = Mathf.Clamp01(value);  } }
    public float GlobalVolume { get => _globalVolume; set { _globalVolume = Mathf.Clamp01(value); UpdateMusicVol(); } }
    private AudioPrefab.Pool _pool;
    public void ManualInject(DiContainer container)
    {
        container.Inject(this);
    }
    [Inject]
        private void InjectData(AudioPrefab.Pool pool,SoundZenMan checkcopy)
        {
        _pool = pool;
        _soundDictionary = new Dictionary<string, Sound>();
        for (int i = 0; i < _sounds.Count; i++)
        {
            _soundDictionary.Add(_sounds[i].key, _sounds[i]);
            _sounds[i].simultaneousPlayCount = 0;
        }
    }


    /// <summary>
    /// play audo at 0,0,0
    /// </summary>
    /// <param name="key">key to sound ref</param>
    /// <param name="autoScaleVolume">scale volume depend on its actual playing number in scene</param>
    /// <param name="maxVolumeScale">max volume value</param>
    public AudioSource PlaySound(string key, bool autoScaleVolume = true, float maxVolumeScale = 1f)
        {
            return CRPlaySound(_soundDictionary[key],Vector3.zero, autoScaleVolume, maxVolumeScale); 

        }
    /// <summary>
    /// play audio at given position
    /// </summary>
    /// <param name="key">key to sound ref</param>
    /// <param name="pos">at what pos to spawn</param>
    /// <param name="autoScaleVolume">scale volume depend on its actual playing number in scene</param>
    /// <param name="maxVolumeScale">max volume value</param>
    public AudioSource PlaySound(string key,Vector3 pos, bool autoScaleVolume = true, float maxVolumeScale = 1f)
    {
       return  CRPlaySound(_soundDictionary[key], pos, autoScaleVolume, maxVolumeScale);
    }
    /// <summary>
    /// sound player and component recycler
    /// </summary>
    /// <param name="sound">sound ref</param>
    /// <param name="pos">at what pos to spawn</param>
    /// <param name="autoScaleVolume">scale volume depend on its actual playing number in scene</param>
    /// <param name="maxVolumeScale">max volume value</param>
    /// <returns>work in time</returns>
    private AudioSource CRPlaySound(Sound sound,Vector3 pos, bool autoScaleVolume = true, float maxVolumeScale = 1f)
        {
            if (sound.simultaneousPlayCount >= maxSimultaneousSounds)
            {
                return null;
            }

            sound.simultaneousPlayCount++;

            float vol = maxVolumeScale* sound.soundVolume* _globalVolume*_soundVolume;

            // Scale down volume of same sound played subsequently
            if (autoScaleVolume && sound.simultaneousPlayCount > 0)
            {
                vol /= (float)(sound.simultaneousPlayCount);
            }
            AudioSource audioSorce = _pool.Spawn(sound,_pool).GetComponent<AudioSource>();
        //audioSorce.transform.SetParent(this.transform);
            audioSorce.mute = IsMuted();
            audioSorce.PlayOneShot(sound.clips[Random.Range(0, sound.clips.Length)], vol);
        return audioSorce;
    }

    /// <summary>
    /// play music- only one music at time
    /// </summary>
    /// <param name="key">key for music ref</param>
    /// <param name="loop">if must loop- automaticly true</param>
    /// <returns>return ref to audio source</returns>
        public AudioSource PlayMusic(string key, Vector3 pos = default(Vector3),bool loop=true)
        {
        _usedMusicKey = key;
        _musicSource = _pool.Spawn(_soundDictionary[key], _pool).GetComponent<AudioSource>();
            _musicSource.clip = _soundDictionary[key].clips[Random.Range(0, _soundDictionary[key].clips.Length)];
            _musicSource.loop = loop;
        _musicSource.volume = _soundDictionary[key].soundVolume * _globalVolume * _musicVolume;
            _musicSource.Play();
            if(IsMuted() || IsMusicOff())
            {
                _musicSource.mute = true;
            }
        return _musicSource;
        }
    //for IToggleAudio
    private void UpdateMusicVol()
    {
        if(_musicSource!=null)
        _musicSource.volume = _soundDictionary[_usedMusicKey].soundVolume * _globalVolume * _musicVolume;
    }
    public bool IsMuted()
        {
            return (PlayerPrefs.GetInt(MUTE_PREF_KEY, UN_MUTED) == MUTED);
        }

        public bool IsMusicOff()
        {
            return (PlayerPrefs.GetInt(MUSIC_PREF_KEY, MUSIC_ON) == MUSIC_OFF);
        }

        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleMute()
        {
            // Toggle current mute status
            
            if (!IsMuted())
            {
                // Muted
                PlayerPrefs.SetInt(MUTE_PREF_KEY, MUTED);
                _musicSource.mute = true;
            }
            else
            {
                // Un-muted
                PlayerPrefs.SetInt(MUTE_PREF_KEY, UN_MUTED);
                _musicSource.mute = IsMusicOff();

            }
        }


        /// <summary>
        /// Toggles the mute status.
        /// </summary>
        public void ToggleMusic()
        {

            if (IsMusicOff())
            {
                PlayerPrefs.SetInt(MUSIC_PREF_KEY, MUSIC_ON);
                _musicSource.mute = IsMuted();
            }
            else
            {
                PlayerPrefs.SetInt(MUSIC_PREF_KEY, MUSIC_OFF);
                _musicSource.mute = true;
            }
        }


    }
