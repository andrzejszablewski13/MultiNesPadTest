using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class ExplosionController : MonoBehaviour
{
    // Start is called before the first frame update
    private ParticleSystem[] _particle;
    private WaitForSeconds _wait;
    public IEnumerator PlayParticle()
    {
        foreach (ParticleSystem item in _particle)
        {
            item.Play();
        }
        yield return _wait;
        SpawnerSystem.Pool.Despawn(this);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public class Pool :MonoMemoryPool<Vector3,ExplosionController>
    {
        protected override void OnCreated(ExplosionController item)
        {
            base.OnCreated(item);
            item._particle = item.GetComponentsInChildren<ParticleSystem>();
            item._wait = new WaitForSeconds(item._particle[0].main.startLifetime.constantMax+1);
        }
        protected override void Reinitialize(Vector3 pos, ExplosionController item)
        {
            base.Reinitialize(pos,item);
            item.transform.position = pos;
            item.StartCoroutine(item.PlayParticle());
        }
    }
}
