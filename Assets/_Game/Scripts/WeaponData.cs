using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
[CreateAssetMenu(fileName ="WeaponData",menuName ="Weapon")]
public class WeaponData : ScriptableObject
{
    [System.Serializable]
    public struct WeaponAttackData
    {
        [SerializeField] private int _dmg,_shootNumberPerClick;
        [SerializeField] private float _knockBack,_delay;
        [Space(40)]
        [SerializeField] private bool _isSpread;
        [SerializeField] private bool  _isOneAlwaysMiddle;
        [SerializeField] private float2 _spreadAngle;
        [Space(40)]
        [SerializeField] private bool _isShootProjectole;
        [SerializeField] private int _hp;
        [SerializeField] private float _lifeTime, _speed;
        [SerializeField] private Projectile _preFab;
        [Space(40)]
        [SerializeField] private bool _isRaycast;
        [SerializeField] private int _distance;
        [SerializeField] private LayerMask _mask;
        [SerializeField] private string _shotSound;

        public int Dmg { get => _dmg; }
        public int ShootNumberPerClick { get => _shootNumberPerClick;}
        public float KnockBack { get => _knockBack;}
        public float Delay { get => _delay;}
        public bool OneAlwaysMiddle { get => _isOneAlwaysMiddle; }
        public float2 SpreadAngle { get => _spreadAngle;}
        public bool IsShootProjectole { get => _isShootProjectole;}
        public float LifeTime { get => _lifeTime;}
        public bool IsRaycast { get => _isRaycast;}
        public float Speed { get => _speed;}
        public Projectile PreFab { get => _preFab;}
        public bool IsSpread { get => _isSpread; }
        public int Distance { get => _distance;}
        public LayerMask Mask { get => _mask;}
        public string ShotSound { get => _shotSound;}
        public int Hp { get => _hp; }
    }
    [SerializeField] private WeaponAttackData _normalAttack, _specialAttack;

    public WeaponAttackData SpecialAttack { get => _specialAttack;}
    public WeaponAttackData NormalAttack { get => _normalAttack;}
    [SerializeField] private WeaponObjectController _weaponPrefab;
    public WeaponObjectController WeaponPrefab { get => _weaponPrefab;}


}
