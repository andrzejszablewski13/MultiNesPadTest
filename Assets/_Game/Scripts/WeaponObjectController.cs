using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponObjectController : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Transform[] _shootPoints;
    private WaitForSeconds _wait;
    private float _sleepTime= .05f;
    private void Awake()
    {
        _wait = new WaitForSeconds(_sleepTime);
    }
    public Transform[] ShootPoints { get => _shootPoints;}
    public Vector3 ShotPoint(int i)
    {
        return ShootPoints[i % _shootPoints.Length].position;
    }
    public void ShotLasers(Vector3 target)
    {
        foreach (Transform item in _shootPoints)
        {
            StartCoroutine(ShowLaser(item.GetComponent<LineRenderer>(), item.InverseTransformPoint(target)));
        }
    }
    public Vector3 ForwardShotPoint(int i)
    {
        return ShootPoints[i % _shootPoints.Length].forward;
    }
    public virtual void RotateTowards(Transform target)
    {
        this.transform.LookAt(target);
    }
    public bool isShowingLaser = false;
    public IEnumerator ShowLaser(LineRenderer renderer,Vector3 target)
    {
        if (isShowingLaser) yield return null;
        renderer.SetPosition(1, target);
        isShowingLaser = true;
        renderer.enabled = true;
        yield return _wait;
        ResetLaser(renderer);
        isShowingLaser = false;
    }
    public void ResetLaser(LineRenderer renderer)
    {
        renderer.enabled = false;
        renderer.SetPosition(1, Vector3.zero);
    }
}
