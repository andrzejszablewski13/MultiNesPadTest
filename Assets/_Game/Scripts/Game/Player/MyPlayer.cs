using Fusion;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using System.Collections.Generic;
using Zenject;
public class MyPlayer : NetworkBehaviour, IHadHP
{
        private Rigidbody _rb;
        private bool _myPlayer = false;
    private NetworkRunner _runner;
    private PlayerLocalController _playerLocal;
    private static int _startHP = 100;
    [Networked] private int _killed { get; set; }
    [Networked] private int _dyied { get; set; }
    public int Killed => _killed;
    public int Dyied => _dyied;
    [Networked(OnChanged = nameof(OnHpValueChanged))] 
        public int Hp { get; set; }
    public void ChangeHP(int value,PlayerRef player)
    {
        lastattackedplayer = player;
        Hp += value;
    }
    [Networked] public PlayerRef lastattackedplayer { get; set; }
        [Networked(OnChanged = nameof(OnLiveStateChange))]
        public NetworkBool Alive { get; set; }
    public bool _wasEverAlive = false;
        public static void OnHpValueChanged(Changed<MyPlayer> changed)
        {
        MyPlayer temp = changed.Behaviour;
        if(temp.Hp> _startHP*2)
        {
            temp.Hp = _startHP * 2;
        }
        if (temp._myPlayer)
        {
            temp._playerLocal.ActualizeUiHealth(temp.Hp, _startHP);
        }
        if (temp.Hp <=0 && temp.Alive )
            {
            temp.Runner.GetPlayerObject(temp.lastattackedplayer)
                .GetBehaviour<MyPlayer>()._killed++;
            temp._dyied++;
            temp.Alive = false;

        }
        }
        public static void OnLiveStateChange(Changed<MyPlayer> changed)
        {
        changed.Behaviour._playerLocal.ActivatePlayerBody(changed.Behaviour.Alive);
            changed.Behaviour.Hp = _startHP;
            if (changed.Behaviour._myPlayer)
            {
                changed.Behaviour._camera.enabled = changed.Behaviour.Alive;
            }
        }
    public static void OnNickChange(Changed<MyPlayer> changed)
    {
        changed.Behaviour._nickText.text = changed.Behaviour.Nick;
    }
    [Networked(OnChanged = nameof(OnNickChange))]

    public string Nick { get; set; }

        [Rpc(sources: RpcSources.InputAuthority, targets: RpcTargets.StateAuthority)]
        public void RPC_SetNick(string nick)
        {
            Nick = nick;
        }
        public override void FixedUpdateNetwork()
        {
        
            if (GetInput(out NetworkInputData data))
            {
            if((data.buttons & NetworkInputData.RESPAWN) != 0)
            {
                Alive = true;
            }
            PlayerControl(data);
            }
        }
    private void PlayerControl(NetworkInputData data)
    {
        if (_playerLocal.AllowMove)
        {
            _rb.velocity += (transform.forward * data.direction.z);
            _rb.angularVelocity += (data.direction.x * PlayerPrefs.GetInt(LobbyCanvas.ROTATIONSEN) * Vector3.up);
            if ((data.buttons & NetworkInputData.SPECIAL) != 0)
            {
                _weapon.Attack(Runner, Object.InputAuthority, true);
            }
            if ((data.buttons & NetworkInputData.ATTACK) != 0)
            {
                _weapon.Attack(Runner, Object.InputAuthority, false);
            }
        }
    }
    private CinemachineVirtualCamera _camera;
    private WeaponController _weapon;
    private ScoreBoardController _scoreBoard;
    private GameCanvasMenager _ui;
    private TMPro.TextMeshPro _nickText;
    [Inject]
    private void InectData(PlayerLocalController playerlocalcontroller, GameCanvasMenager ui,
        CinemachineVirtualCamera camera, WeaponController weapon, ScoreBoardController scoreboardController,
        Rigidbody rb, TMPro.TextMeshPro nickText)
        {
        _playerLocal = playerlocalcontroller;
        _camera = camera;
        _weapon = weapon;
        _rb = rb;
        _ui = ui;
        _nickText = nickText;

        _scoreBoard = scoreboardController;
        }
        public override void Spawned()
        {
        if (Object.HasInputAuthority)
            {   
                RPC_SetNick(PlayerPrefs.GetString(LobbyCanvas.NICK));
                _myPlayer = true;
            _playerLocal.localPlayer = true;
            _ui.ExitRoom.onClick.AddListener(LeaveRoom);
            Hp = _startHP;
        }
        Alive = true;
        Runner.SetPlayerObject(Object.InputAuthority, Object);

        _runner = Runner;
        _scoreBoard.SetRunner(_runner);

        }
    private void LeaveRoom()
    {
        _runner.Shutdown();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
