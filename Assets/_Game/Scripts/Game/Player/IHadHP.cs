﻿using Fusion;
public interface IHadHP
{
    public void ChangeHP(int value,PlayerRef player);
    public int Hp { get; set; }
    public PlayerRef lastattackedplayer { get; set; }
}