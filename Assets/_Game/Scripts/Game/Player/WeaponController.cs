using Unity.Mathematics;
using UnityEngine;
using Fusion;
using Zenject;
public class WeaponController : NetworkBehaviour
{
    private WeaponData[] Weapons;
    private PlayerMiddlePoint _point;
    private PlayerLocalController _playerController;
    private WeaponHandle _handle;
    private WeaponObjectController _weapon=null;
    [Networked]private TickTimer SpecialAttackDelay { get; set; }
    [Networked] private TickTimer NormalAttackDelay { get; set; }
    [Networked] private int ActiveWeapon { get; set; }
    private IAudioPlayer _playerAudio;
    [Networked] private int _projectileIndex { get; set; }
    [Inject]
    private void InjectData(WeaponData[] weapon, PlayerMiddlePoint point, PlayerLocalController playerController,
      WeaponHandle handle, IAudioPlayer playerAudio)
    {
        Weapons = weapon;
        _point = point;
        _handle = handle;
        _playerAudio = playerAudio;
        _playerController = playerController;
    }
    public bool SetNewWeapon(WeaponData weapon)
    {
        for (int i = 0; i < Weapons.Length; i++)
        {
            if (weapon == Weapons[i])
            {

                return SetNewWeapon(i);
            }
        }
        return false;
    }
    private string _pickUpWeaponSound = "GunPickUp";
    public bool SetNewWeapon(int i)
    {
        if(i==ActiveWeapon)
        {
            return false;
        }
        ActiveWeapon = i;
        SpecialAttackDelay = TickTimer.None;
        NormalAttackDelay = TickTimer.None;
        _playerAudio.PlaySound(_pickUpWeaponSound);
        _projectileIndex = 0;
        SetWeaponPrefab();
        return true;
    }
    private void Start()
    {
        SetWeaponPrefab();
    }
    private void SetWeaponPrefab()
    {
        if (_weapon != null)
        {
            Destroy(_weapon.gameObject);
        }
        _weapon = Instantiate(Weapons[ActiveWeapon].WeaponPrefab.gameObject, _handle.transform)
            .GetComponent<WeaponObjectController>();
        _weapon.RotateTowards(_point.transform);
    }
    public void Attack(NetworkRunner runner,PlayerRef playerref,bool specialattack=false)
    {
        if(_weapon != null && !GiveTimer(specialattack).ExpiredOrNotRunning(runner))
        {
            return;
        }
        WeaponData.WeaponAttackData data = specialattack ? Weapons[ActiveWeapon].SpecialAttack : Weapons[ActiveWeapon].NormalAttack;
        if (specialattack)
        {
            SpecialAttackDelay = TickTimer.CreateFromSeconds(runner, data.Delay);
        }
        else
        {
            NormalAttackDelay = TickTimer.CreateFromSeconds(runner, data.Delay);
        }
        LagCompensatedHit hit;
        if (data.IsRaycast)
        {
            for (int i = 0; i < data.ShootNumberPerClick; i++)
            {

                hit=new LagCompensatedHit();
                runner.LagCompensation.Raycast(_weapon.ShotPoint(i),
                    CalculateAngle(data,i)* 
                    (_point.transform.position - _weapon.ShotPoint(i)).normalized,
                    data.Distance, playerref, out hit, data.Mask);
                
                if (hit.Hitbox != null)
                {
                    _weapon.ShotLasers(hit.Point);
                    hit.GameObject.GetComponentInChildren<ITakeDMG>()
                        .HIt(playerref,data.Dmg, hit.Point, data.KnockBack);
                }
                else
                {
                    _weapon.ShotLasers(_point.transform.position);
                }
            }

        }
        
        if(data.IsShootProjectole)
        {
            for (int i = 0; i < data.ShootNumberPerClick; i++)
            {
                runner.Spawn(data.PreFab,
                    _weapon.ShotPoint(i) + _weapon.ForwardShotPoint(i),
                     Quaternion.LookRotation(CalculateAngle(data, i) * 
                     (_point.transform.position - _weapon.ShotPoint(i)).normalized),
                    playerref,
                    (runner, o) =>
                    {
                        o.GetComponent<Projectile>().Init(data, playerref, _playerAudio, _projectileIndex);
                    });
            }
            _projectileIndex++;
        }

        _playerAudio.PlaySound(data.ShotSound);
    }
    private TickTimer GiveTimer(bool specialattack)
    {
        return specialattack ? SpecialAttackDelay : NormalAttackDelay;
    }
    private Quaternion CalculateAngle(WeaponData.WeaponAttackData data, int i)
    {
        Quaternion rotation;
        if (!data.IsSpread || (i == 0 && data.OneAlwaysMiddle))
        {
            rotation = Quaternion.Euler(Vector3.zero);
        }
        else
        {
            float2 spreadangle = _playerController.RanGen.NextFloat2(data.SpreadAngle);
            rotation = Quaternion.Euler(spreadangle.x, spreadangle.y, 0);
        }
        return rotation;
    }
}
