using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Fusion;
using Cinemachine;
public class PlayerInstaller : MonoInstaller<PlayerInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<Rigidbody>()
            .FromComponentSibling()
            .AsSingle();
        Container.Bind<PlayerLocalController>()
            .FromComponentSibling()
            .AsSingle();
        Container.Bind<WeaponController>()
            .FromComponentSibling()
            .AsSingle();

        Container.Bind<HitboxRoot>()
            .FromComponentSibling()
            .AsSingle();
        Container.BindInterfacesAndSelfTo<MyPlayer>()
            .FromComponentSibling()
            .AsSingle();
        Container.Bind<PlayerMiddlePoint>()
            .FromComponentsInChildren(false, null, true)
            .AsSingle();
        Container.Bind<WeaponHandle>()
            .FromComponentInChildren()
            .AsSingle();
        Container.Bind<TMPro.TextMeshPro>()
            .FromComponentInChildren()
            .AsSingle();
        Container.Bind<CinemachineVirtualCamera>()
            .FromComponentsInChildren(false,null,true)
            .AsSingle();
        Container.Bind<MeshRenderer>()
            .FromComponentsInChildren(false, null, true)
            .AsSingle();
        Container.Bind<Collider>()
            .FromComponentsInChildren(false, null, true)
            .AsSingle();
    }
}
