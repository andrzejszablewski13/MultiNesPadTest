using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = Unity.Mathematics.Random;
using Fusion;
using DG.Tweening;
public class PlayerLocalController : MonoBehaviour
{
    [HideInInspector] public bool localPlayer=false;
    private List<SpawnerPoint> _spawnerPoints;
    private Random _ranGen;

    public List<SpawnerPoint> SpawnerPoints { get => _spawnerPoints;}
    public Random RanGen { get => _ranGen;}
    public Vector3 RandPos { get => SpawnerPoints[
            _ranGen.NextInt(0, SpawnerPoints.Count)]
            .transform.position;
    }
    public bool AllowMove { get => _allowMove;}

    private bool _allowMove;

    private HitboxRoot _hitboxRoot;
    private GameCanvasMenager _ui;
    private Rigidbody _rb;
    private ScoreBoardController _scoreboardController;
    private IAudioPlayer _audioPlayer;
    private float _fadeTime = 1.5f;
    [Inject]
    public void InjectData(List<SpawnerPoint> spawnerPoints,
        HitboxRoot hitboxRoot,GameCanvasMenager ui, Rigidbody rb,
        ScoreBoardController scoreboardController, IAudioPlayer audioPlayer
       )
    {
        _audioPlayer = audioPlayer;
        _ranGen.InitState();
        _spawnerPoints = spawnerPoints;

        _hitboxRoot = hitboxRoot;
        _scoreboardController = scoreboardController;
        _ui = ui;
        _rb = rb;
        _ui.ExitGame.onClick.AddListener(Application.Quit);
        _ui.BlackBoard.DOFade(0, _fadeTime);
    }
    public void Respawn()
    {
        this.transform.position = RandPos;
        ActivatePlayerBody();
    }
    public void Death()
    {
        ActivatePlayerBody(false);
    }
    public void ActualizeUiHealth(int value,int maxhp)
    {
        float divided = (float)value / maxhp;
        Debug.Log(divided);
        _ui.HpGraphic.fillAmount = Mathf.Clamp01(divided);
        _ui.HpText.text = value.ToString();
        if(divided>1)
        {
            _ui.HpAdditionalGraphic.fillAmount = Mathf.Clamp01(divided - 1);
        }else
        {
            _ui.HpAdditionalGraphic.fillAmount = 0;
        }
    }
    private string _deathSound = "Death";
    public void ActivatePlayerBody(bool active=true)
    {
        if (localPlayer)
        {
            _ui.ScoreBoard.SetActive(!active);
            if(!active)
            {
                _scoreboardController.SetScoreBoard();
            }
            _ui.DeathScreen.SetActive(!active);
        }
        if(!active)
        {
            _audioPlayer.PlaySound(_deathSound);
            _ui.Respawn.Select();
            _rb.isKinematic = !active;
            _allowMove = active;
            _hitboxRoot.HitboxRootActive = active;
            foreach (Collider item in this.GetComponentsInChildren<Collider>())
            {
                item.enabled = active;
            }
        }else
        {
            StartCoroutine(SlowerStart());
            
        }

        foreach (MeshRenderer item in this.GetComponentsInChildren<MeshRenderer>())
        {
            item.enabled = active;
        }
        foreach (SkinnedMeshRenderer item in this.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            item.enabled = active;
        }

    }
    private IEnumerator SlowerStart()
    {
        yield return new WaitForSeconds(_fadeTime);
        _rb.isKinematic = false;
        _hitboxRoot.HitboxRootActive = true;
        _allowMove = true;
        foreach (Collider item in this.GetComponentsInChildren<Collider>(true))
        {
            item.enabled = true;
        }
    }
}
