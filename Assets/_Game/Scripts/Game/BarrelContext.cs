using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Fusion;
public class BarrelContext : MonoInstaller<BarrelContext>
{
    public override void InstallBindings()
    {
        Container.Bind<Rigidbody>()
    .FromComponentSibling()
    .AsSingle();
        Container.Bind<HitboxRoot>()
    .FromComponentSibling()
    .AsSingle();
        Container.BindInterfacesAndSelfTo<Barrel>()
    .FromComponentSibling()
    .AsSingle();
    }
}
