using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
public class WeaponSpawner : NetworkBehaviour
{

    [Networked (OnChanged = nameof(OnTimerChange))] TickTimer Timer { get; set; }
    [SerializeField] private WeaponData _weaponSpawned=null;
    [SerializeField] private float _coolDownTime = 1f,_rotateSpeed=5f,_scaleMultiplayer=3;
    private GameObject _weaponView;
    private static void OnTimerChange(Changed<WeaponSpawner> data)
    {
        data.Behaviour._weaponView.SetActive(
            data.Behaviour.Timer.ExpiredOrNotRunning(data.Behaviour.Runner));
    }
    public override void FixedUpdateNetwork()
    {
        if (!Timer.ExpiredOrNotRunning(Runner))
        {
            return;
        }
            if (!_weaponView.activeSelf)
            {
                _weaponView.SetActive(true);
            }
            _weaponView.transform.Rotate(Vector3.up, _rotateSpeed * Runner.DeltaTime);
    }
    public override void Spawned()
    {
        base.Spawned();
        transform.GetChild(0).GetChild(0).localScale = new Vector3(
            1 / transform.GetChild(0).localScale.x,
            1 / transform.GetChild(0).localScale.y, 
            1 / transform.GetChild(0).localScale.z
            );
        _weaponView = Instantiate(_weaponSpawned.WeaponPrefab.gameObject
            , transform.GetChild(0).GetChild(0));
        _weaponView.transform.localScale *= _scaleMultiplayer;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(_weaponView.activeSelf && other.attachedRigidbody!= null
            && other.attachedRigidbody.TryGetComponent(out WeaponController controller)
            &&  Timer.ExpiredOrNotRunning(Runner))
        {
            if(controller.SetNewWeapon(_weaponSpawned))
            {
                Timer = TickTimer.CreateFromSeconds(Runner,_coolDownTime);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (_weaponView.activeSelf && other.attachedRigidbody != null
    && other.attachedRigidbody.TryGetComponent(out WeaponController controller)
    && Timer.ExpiredOrNotRunning(Runner))
        {
            if (controller.SetNewWeapon(_weaponSpawned))
            {
                Timer = TickTimer.CreateFromSeconds(Runner, _coolDownTime);
            }
        }
    }
}
