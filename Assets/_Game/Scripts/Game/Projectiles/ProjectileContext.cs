using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Fusion;
public class ProjectileContext : MonoInstaller<ProjectileContext>
{
    public override void InstallBindings()
    {
        Container.Bind<HitboxRoot>().FromComponentSibling().AsSingle();
        Container.BindInterfacesAndSelfTo<Projectile>().FromComponentSibling().AsSingle();
        Container.Bind<Rigidbody>().FromComponentSibling().AsSingle();
    }
}
