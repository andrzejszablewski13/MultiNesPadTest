using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
public class Projectile : NetworkBehaviour, IHadHP
{
    [Networked] private TickTimer Life { get; set; }
    protected int _index=0;
    protected int _dmg=5;
    protected float _speed=25;
    protected float _knockBack=5;
    protected IAudioPlayer _audioPlayer=null;
    [Networked] protected PlayerRef playerspawn { get; set; }
    [Networked(OnChanged = nameof(OnHpValueChanged))]
    public int Hp { get; set; }
    public static void OnHpValueChanged(Changed<Projectile> changed)
    {
        if (changed.Behaviour.Hp <= 0)
        {
            changed.Behaviour.DoDmg(changed.Behaviour.transform.position);
            changed.Behaviour.Runner.Despawn(changed.Behaviour.Object, true);
        }
    }
    public PlayerRef lastattackedplayer { get; set; }

    [SerializeField] protected string _soundEnd= "ExplosionBarrel";
    public void Init(WeaponData.WeaponAttackData data, PlayerRef player,IAudioPlayer audioPlayer,int indnex)
    {
        Hp = data.Hp;
        _index = indnex;
        Life = TickTimer.CreateFromSeconds(Runner, data.LifeTime);
        playerspawn = player;
        _dmg = data.Dmg;
        _knockBack = data.KnockBack;
        _speed = data.Speed;
        _audioPlayer = audioPlayer;
    }
    public override void FixedUpdateNetwork()
    {
        if (Life.Expired(Runner))
        {
            Runner.Despawn(Object);
            return;
        }
        else
            transform.position += _speed * transform.forward * Runner.DeltaTime;
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.TryGetComponent<Projectile>(out var projectile))
        {
            if(projectile.playerspawn==playerspawn && projectile._index==_index)
            {
                return;
            }
        }
        if (other.gameObject.TryGetComponent<ITakeDMG>(out var temp))
        {
            DoDmg(other.ClosestPoint(this.transform.position), temp);
        }
        DoAreaDmg(other.ClosestPoint(this.transform.position));
        if (Object!=null)
        Runner.Despawn(Object, true);
    }
    protected virtual void DoDmg(Vector3 point, ITakeDMG target =null)
    {
        _audioPlayer.PlaySound(_soundEnd);
        if (target != null)
        {
            if (!lastattackedplayer.IsNone)
            {
                target.HIt(lastattackedplayer, _dmg, point, _knockBack);
            }else
            {
                target.HIt(playerspawn, _dmg, point, _knockBack);
            }
        }
        SpawnerSystem.Pool.Spawn(point);
    }
    protected virtual void DoAreaDmg(Vector3 point)
    {
    }

    public void ChangeHP(int value, PlayerRef player)
    {
        lastattackedplayer = player;
        Hp += value;
    }
}
