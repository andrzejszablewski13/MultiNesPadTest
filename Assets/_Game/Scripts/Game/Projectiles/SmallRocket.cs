using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
public class SmallRocket : Projectile
{
    [SerializeField] private float _rotationSpeed = 5;
    public override void FixedUpdateNetwork()
    {
        base.FixedUpdateNetwork();
        if(Runner!=null)
        this.transform.Rotate(_rotationSpeed * Runner.DeltaTime * Vector3.back);
    }
    private List<LagCompensatedHit> hits;
    protected override void DoAreaDmg(Vector3 point)
    {
        if (!lastattackedplayer.IsNone)
        {
            playerspawn = lastattackedplayer;
        }
        //base.DoDmg(point, target);
        _audioPlayer.PlaySound(_soundEnd);
        hits = new List<LagCompensatedHit>();
        Runner.LagCompensation.OverlapSphere(point, 3, playerspawn, hits, 2048);
        foreach (LagCompensatedHit item in hits)
        {
            if (item.Hitbox.TryGetComponent<ITakeDMG>(out ITakeDMG hitted))
            {
                hitted.HIt(playerspawn, _dmg, point, _knockBack);
            }
        }
        SpawnerSystem.Pool.Spawn(point);
    }
    protected override void DoDmg(Vector3 point, ITakeDMG target = null)
    {

    }
}
