using UnityEngine;
using Fusion;

public class PhysxBall : NetworkBehaviour
{
    [Networked] private TickTimer life { get; set; }
    [SerializeField] private float _startLifeTime = 5;
    public void Init(Vector3 forward)
    {
        life = TickTimer.CreateFromSeconds(Runner, _startLifeTime);
        GetComponent<Rigidbody>().velocity = forward;
    }

    public override void FixedUpdateNetwork()
    {
        if (life.Expired(Runner))
            Runner.Despawn(Object);
    }
}