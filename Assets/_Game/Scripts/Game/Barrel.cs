using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using Zenject;
public class Barrel : NetworkBehaviour,IHadHP
{
    [Networked(OnChanged = nameof(OnHpValueChanged))]
    public int Hp { get; set; }
    private HitboxRoot _hitBoxRoot;
    [SerializeField]private float _respawnTime = 60f;
    private bool _visible = true;
    [Networked]
    private TickTimer _tickTimer { get; set; }
    public static void OnHpValueChanged(Changed<Barrel> changed)
    {
        if (changed.Behaviour.Hp <= 0 && changed.Behaviour._visible)
        {
            changed.Behaviour.Explode();
        }
    }
    public PlayerRef lastattackedplayer { get; set; }
    public void ChangeHP(int value,PlayerRef player)
    {
        Hp += value;
        lastattackedplayer = player;
    }
    private IAudioPlayer _playerAudio;
    [Inject]
    private void InjectData(IAudioPlayer playerAudio)
    {
        _playerAudio = playerAudio;
    }
    private int _startHP = 10;
    public override void Spawned()
    {
        Hp = _startHP;
        
    }
    private List<LagCompensatedHit> _hits=new List<LagCompensatedHit>();
    public override void FixedUpdateNetwork()
    {
        if(!_visible && _tickTimer.Expired(Runner))
        {
            Hp = _startHP;
            Visibility(true);
        }
    }
    private float _radius = 15, _knockBack = 150;
    private int _mask = 2048, _dmg = 15;
    private string _sound = "ExplosionBarrel";
    public void Explode()
    {
        _playerAudio.PlaySound(_sound);
      int lenght= Runner.LagCompensation
            .OverlapSphere(this.transform.position, _radius, lastattackedplayer, _hits, _mask);
        
        for (int i = 0; i < lenght; i++)
        {
            if(_hits[i].Hitbox.TryGetComponent(out ITakeDMG killable))
            {
                killable.HIt(lastattackedplayer, _dmg, this.transform.position, _knockBack);
            }

        }
        SpawnerSystem.Pool.Spawn(this.transform.position);
        _tickTimer = TickTimer.CreateFromSeconds(Runner, _respawnTime);
        Visibility(false);
    }
    private void Visibility(bool istrue=true)
    {
        _visible = istrue;
        foreach (MeshRenderer item in this.GetComponentsInChildren<MeshRenderer>(true))
        {
            item.enabled = istrue;
        }
        foreach (Collider item in this.GetComponentsInChildren<Collider>(true))
        {
            item.enabled = istrue;
        }
        this.GetBehaviour<HitboxRoot>().HitboxRootActive = istrue;
        this.GetBehaviour<Hitbox>().HitboxActive = istrue;
    }
}
