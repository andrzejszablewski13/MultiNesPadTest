using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;
public class PlayerScoreboard : MonoBehaviour
{
    // Start is called before the first frame update
    private string[] _texts;
    private TextMeshProUGUI[] _textfields;
    private void SetStrings()
    {
        for (int i = 0; i < _textfields.Length; i++)
        {
            _textfields[i].text = _texts[i % _textfields.Length];
        }
    }
    
    public class Pool:MonoMemoryPool<string[], PlayerScoreboard>
    {
        protected override void OnCreated(PlayerScoreboard item)
        {
            base.OnCreated(item);
            item._textfields = item.GetComponentsInChildren<TextMeshProUGUI>(true);
        }
        protected override void Reinitialize(string[] p1, PlayerScoreboard item)
        {
            base.Reinitialize(p1, item);
    
            item._texts =p1;

            item.SetStrings();
        }
    }
}
