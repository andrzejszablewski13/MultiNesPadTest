using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using Zenject;
public class HealthPickUp : NetworkBehaviour
{
    // Start is called before the first frame update
    [Networked] private TickTimer _cd { get; set; }
    [Networked(OnChanged = nameof(OnActiveStateChange))] private NetworkBool _activeState { get; set; }
    [SerializeField]private float _coolDownTime = 60,_rotateSpeed=90;
    [SerializeField] private int _healingValue=25;
    private Collider _collider;
    private bool _spawned = false;
    private Vector3 _startPos;
    private IAudioPlayer _audioPlayer;
    [Inject]
    private void InjectData(IAudioPlayer audioPlayer)
    {
        _audioPlayer = audioPlayer;
    }
    private static void OnActiveStateChange(Changed<HealthPickUp> changed)
    {
        HealthPickUp beh = changed.Behaviour;
        beh._collider.enabled = beh._activeState;
        beh.transform.GetChild(0).gameObject.SetActive(beh._activeState);
        if(!beh._activeState && beh._cd.ExpiredOrNotRunning(beh.Runner))
        {
            beh._cd = TickTimer.CreateFromSeconds(beh.Runner, beh._coolDownTime);
        }
    }
    public override void Spawned()
    {
        base.Spawned();
        _collider = this.GetComponent<Collider>();
        _activeState = !(_cd.IsRunning && _activeState);
        _collider.enabled = _activeState;
        _startPos = this.transform.position;
    }
    public override void FixedUpdateNetwork()
    {
        if (_activeState)
        {
            this.transform.Rotate(_rotateSpeed * Runner.DeltaTime * Vector3.up);
            this.transform.position = _startPos + Vector3.up * Mathf.PerlinNoise(Runner.SimulationRenderTime% _rotateSpeed, _startPos.z);
        }
        else
         if (_cd.Expired(Runner))
        {
            _activeState = true;
        }
        
    }
    private string _sound = "HealthPickUp";
    private void OnTriggerEnter(Collider other)
    {
        if(_activeState && other.attachedRigidbody!=null && other.attachedRigidbody.TryGetComponent<MyPlayer>(out var player))
        {
            player.Hp += _healingValue;
            _audioPlayer.PlaySound(_sound);
            _activeState = false;
        }
    }
}
