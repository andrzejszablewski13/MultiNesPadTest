using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class GameCanvasMenager : MonoBehaviour
{
    [SerializeField] private Image _blackBoard;
    [Space(40)]
    [SerializeField] private GameObject _deathScreen;
    [SerializeField] private Button _respawn, _exitRoom, _exitGame;
    [Space(40)]
    [SerializeField] private GameObject _scoreBoard;
    [SerializeField] private GameObject _content;
    [Space(40)]
    [SerializeField] private GameObject _hp;
    [SerializeField] private Image _hpGraphic,_hpAdditionalGraphic;
    [SerializeField] private TextMeshProUGUI _hpText;
    public Button Respawn { get => _respawn; }
    public Button ExitRoom { get => _exitRoom;  }
    public Button ExitGame { get => _exitGame; }
    public GameObject DeathScreen { get => _deathScreen;}
    public GameObject ScoreBoard { get => _scoreBoard;}
    public GameObject Content { get => _content;}
    public Image BlackBoard { get => _blackBoard;}
    public GameObject Hp { get => _hp;}
    public Image HpGraphic { get => _hpGraphic;}
    public TextMeshProUGUI HpText { get => _hpText;}
    public Image HpAdditionalGraphic { get => _hpAdditionalGraphic;}
}
