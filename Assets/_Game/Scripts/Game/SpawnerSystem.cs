using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
public class SpawnerSystem : MonoBehaviour
{
    [SerializeField] private NetworkPrefabRef _playerPrefab;
    [SerializeField] private GameObject _spawnerList;
    private List<SpawnerPoint> _spawnerPoints;
    private List<PlayerRef> _spawnedCharacters = new List<PlayerRef>();
    private SignalBus _signalBus = null;
    private static ExplosionController.Pool _pool;

    public static ExplosionController.Pool Pool { get => _pool; }

    [Inject]
    private void InjectData(SignalBus bus, List<SpawnerPoint> spawnerPoints, ExplosionController.Pool pool)
    {
        _signalBus = bus;
        _spawnerPoints = spawnerPoints;
        _pool = pool;
        _signalBus.Subscribe<OnPlayerJoinSignal>(PlayerJoined);
        _signalBus.Subscribe<OnPlayerLeftSignal>(PlayerLeft);
    }

    private void PlayerJoined(OnPlayerJoinSignal data)
    {
        if (_spawnedCharacters.Contains(data.Player))
            return;
            _spawnedCharacters.Add(data.Player);

        if (data.Runner.LocalPlayer!=data.Player && data.Runner.GameMode==GameMode.Shared)
           return;
        // Create a unique position for the player
        Vector3 spawnPosition = _spawnerPoints[data.Player.RawEncoded % _spawnerList.transform.childCount].transform.position;
        NetworkObject networkPlayerObject = data.Runner.Spawn(_playerPrefab, spawnPosition, Quaternion.identity, data.Player);
        
        
    }
    private void PlayerLeft(OnPlayerLeftSignal data)
    {
        if(data.Runner.TryGetPlayerObject(data.Player,out var player))
        {
            Debug.Log(player);
            data.Runner.Despawn(player,true);
            _spawnedCharacters.Remove(data.Player);
            Destroy(player);
        }
        else
        // Find and remove the players avatar
        if (_spawnedCharacters.Contains(data.Player))
        {
            data.Runner.Despawn(data.Runner.GetPlayerObject(data.Player));
            _spawnedCharacters.Remove(data.Player);
        }
    }

}
