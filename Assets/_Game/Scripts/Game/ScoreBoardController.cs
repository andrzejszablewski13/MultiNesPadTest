using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Fusion;
public class ScoreBoardController : MonoBehaviour
{
    private GameCanvasMenager _refCanvas;
    private NetworkRunner _runner;
    private PlayerScoreboard.Pool _pool;
    private List<MyPlayer> _players;
    private ISortPlayersByKD _sorter;
    private List<PlayerScoreboard> _playerStatInstances;
    [Inject]
    private void InjectData(GameCanvasMenager refCanvas,PlayerScoreboard.Pool pool)
    {
        _refCanvas = refCanvas;
        _pool = pool;
        _sorter = new ISortPlayersByKD();
        _playerStatInstances = new List<PlayerScoreboard>();
    }
    public void SetRunner(NetworkRunner runner)
    {
        _runner = runner;
    }
    private void GetPlayers()
    {
        _players = new List<MyPlayer>();
        foreach (PlayerRef item in _runner.ActivePlayers)
        {
            _players.Add(_runner.GetPlayerObject(item).GetBehaviour<MyPlayer>());
        }
        _players.Sort(_sorter);
    }
    public void ClearScoreBoard()
    {
        for (int i = 0; i < _playerStatInstances.Count; i++)
        {
            _pool.Despawn(_playerStatInstances[i]);
        }
        _playerStatInstances.Clear();
    }
    public void SetScoreBoard()
    {
        ClearScoreBoard();
        GetPlayers();
        _refCanvas.Content.GetComponent<RectTransform>().sizeDelta = new Vector2
            (_refCanvas.Content.GetComponent<RectTransform>().sizeDelta.x,
           _players.Count * 101);
        for (int i = 0; i < _players.Count; i++)
        {
            _playerStatInstances.Add( _pool.Spawn(
                new string[]{ _players[i].Nick.ToString(),
                    _players[i].Killed.ToString(),_players[i].Dyied.ToString() }
                ));
            _playerStatInstances[i].transform.SetParent(_refCanvas.Content.transform);

        }
    }
}
public class ISortPlayersByKD : IComparer<MyPlayer>
{
    public int Compare(MyPlayer x, MyPlayer y)
    {
        float xKD = (float)x.Killed / (float)x.Dyied;
        float yKD = (float)y.Killed / (float)y.Dyied;
        return (int)(xKD / yKD - 1);
    }
}