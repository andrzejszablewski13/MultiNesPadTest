﻿using UnityEngine;
using Fusion;

public interface ITakeDMG
{
    public void HIt(PlayerRef player, int value = 1, Vector3 hitpoint = default(Vector3), float knockbackpower = 0);
}