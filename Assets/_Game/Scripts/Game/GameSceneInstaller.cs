using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameSceneInstaller : MonoInstaller
{
    [SerializeField] private GameObject _spawnerList;
    public override void InstallBindings()
    {
        Container.Bind<SpawnerPoint>()
    .FromMethodMultiple(GetSpawnerPoints)
    .AsSingle();
        Container.BindMemoryPool<PlayerScoreboard, PlayerScoreboard.Pool>()
            .FromComponentInNewPrefabResource("PlayerStat")
            .UnderTransform(this.transform);
        Container.BindMemoryPool<ExplosionController, ExplosionController.Pool>()
            .FromComponentInNewPrefabResource("Explosion")
            .UnderTransform(this.transform);
        Container.Bind<ScoreBoardController>()
            .FromComponentInHierarchy()
            .AsSingle();
    }

    private IEnumerable<SpawnerPoint> GetSpawnerPoints(InjectContext context)
    {
        return _spawnerList.GetComponentsInChildren<SpawnerPoint>();
    }
}