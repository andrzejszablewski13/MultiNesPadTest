using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Fusion;
public class TakeDMG : MonoBehaviour,ITakeDMG
{
    // Start is called before the first frame update
    private IHadHP _player;
    private Rigidbody _rb;
    [Inject]
    private void InjectData(IHadHP player,Rigidbody rb)
    {
        _player = player;
        _rb = rb;
    }
    public void HIt(PlayerRef player,
        int value=1,Vector3 hitpoint=default(Vector3),float knockbackpower=0)
    {
        _player.Hp-=value;
        Debug.Log(_player.Hp);
        _player.lastattackedplayer = player;
        if (knockbackpower > 0)
        {
            _rb.AddForceAtPosition(
                 (this.transform.position - hitpoint).normalized * knockbackpower,
                 hitpoint, ForceMode.Impulse);
        }
    }
}
