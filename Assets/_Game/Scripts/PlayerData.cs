using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
[CreateAssetMenu(fileName ="PlayerData",menuName ="PlayerData")]
public class PlayerData : ScriptableObject
{
    public string nick;
}
