using UnityEngine;
using Zenject;
using Fusion;
using System.Collections.Generic;
public class ProjectIntaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<InputMap>().AsSingle();
        Container.Bind<PlayerData>().FromResources("Data");
        Container.Bind<WeaponData>().FromResources("Weapon");
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<OnSessionListUpdatedSignal>().OptionalSubscriber();
        Container.DeclareSignal<OnPlayerJoinSignal>().OptionalSubscriber();
        Container.DeclareSignal<OnPlayerLeftSignal>().OptionalSubscriber();
        Container.DeclareSignal<OnPlayerInputSignal>().OptionalSubscriber();
        Container.BindFactory<RunnerNetworkFactory, RunnerNetworkFactory.Factory>()
            .FromComponentInNewPrefabResource("NetworkRunnerGameobject");
        Container.BindMemoryPool<AudioPrefab, AudioPrefab.Pool>()
    .FromComponentInNewPrefabResource("AudioPrefab")
    //.UnderTransform(this.transform)
    ;
        Container.BindInterfacesAndSelfTo<SoundZenMan>()
            .FromResource("AudioController").AsSingle()
            .OnInstantiated(
            (InjectContext context, SoundZenMan zen) => { Debug.Log("test"); zen.ManualInject(Container); })
            
            .NonLazy();
    }

}

public class OnSessionListUpdatedSignal
{
    public OnSessionListUpdatedSignal(NetworkRunner runner, List<SessionInfo> sessionList)
    {
        Runner = runner;
        SessionList = sessionList;
    }
    public NetworkRunner Runner { get; private set; }
    public List<SessionInfo> SessionList { get; private set; }
}
public class OnPlayerJoinSignal
{
    public OnPlayerJoinSignal(NetworkRunner runner, PlayerRef player)
    {
        Runner = runner;
        Player = player;
    }
    public NetworkRunner Runner { get; private set; }
    public PlayerRef Player { get; private set; }
}
public class OnPlayerLeftSignal
{
    public OnPlayerLeftSignal(NetworkRunner runner, PlayerRef player)
    {
        Runner = runner;
        Player = player;
    }
    public NetworkRunner Runner { get; private set; }
    public PlayerRef Player { get; private set; }
}
public class OnPlayerInputSignal
{
    public OnPlayerInputSignal(NetworkRunner runner, NetworkInput input)
    {
        Runner = runner;
        Input = input;
    }
    public NetworkRunner Runner { get; private set; }
    public NetworkInput Input { get; private set; }
}
