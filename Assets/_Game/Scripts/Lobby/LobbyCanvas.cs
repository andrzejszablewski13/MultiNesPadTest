using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Fusion.Sockets;
using System;
using Zenject;
using DG.Tweening;
public class LobbyCanvas : MonoBehaviour
{
    private NetworkRunner _runner;
    [SerializeField] private Button _host,_setPlayerName,_setRoomName;
    [SerializeField] private GameObject  _content;
    [SerializeField] private GameObject _roomButtonPrefab;
    [SerializeField] private TextMeshProUGUI _statusText, _playerCountText;
    [SerializeField]
    private TextMeshProUGUI _playerNick, _playerBackNick, _roomName, _roomBackName;
    [SerializeField] private Button _exit;

    [SerializeField] private Slider _slider,_sensivitySlider,_audioSlider,_musicSlider,_soundSlider;
    private KeyboardMenager _keyboardMenager;
    [SerializeField] private Image _welcomeImage;
    [SerializeField] private UnityEngine.InputSystem.UI.InputSystemUIInputModule _eventSystemDet;
    private SignalBus _signalBus = null;
    private PlayerData _playerData;
    private int _playerCount=10;
    private IAudioPlayer _playAudio;
    private IAudioVolume _volAudio;
    public const string ROOMNAME= "RoomName", NICK= "Nick",ROTATIONSEN="RotationSensivity";
    [Inject]
    private void InjectData(SignalBus bus,PlayerData _dataP, SoundZenMan _play,DiContainer container)
    {
        _signalBus = bus;
        _playerData = _dataP;
        _signalBus.Subscribe<OnSessionListUpdatedSignal>(OnSessionListUpdated);
        _play.ManualInject(container);
        _playAudio = _play;
        _volAudio = _play;
        SetAudioSliders();
        _signalBus.Subscribe<OnNetworkRunnerCreated>(SetRunner);
    }
    private void SetAudioSliders()
    {
        _audioSlider.onValueChanged.AddListener((float val) => { _volAudio.GlobalVolume = val; });
        _audioSlider.value = _volAudio.GlobalVolume;
        _musicSlider.onValueChanged.AddListener((float val) => { _volAudio.MusicVolume = val; });
        _musicSlider.value = _volAudio.MusicVolume;
        _soundSlider.onValueChanged.AddListener((float val) => { _volAudio.SoundVolume = val; });
        _soundSlider.value = _volAudio.SoundVolume;
    }
    private const string _clickSoundKey = "Click", _musicSoundKey= "Music";
    private void SetRunner(OnNetworkRunnerCreated data)
    {
        _runner = data.Runner;
        _signalBus.Unsubscribe<OnNetworkRunnerCreated>(SetRunner);
    }
    private void SetPlayerCount(int value)
    {
        _playerCount = value;
        _playerCountText.text = value.ToString();
        _playAudio.PlaySound(_clickSoundKey);
    }
    private void SetRotationSen(float value)
    {
        PlayerPrefs.SetInt(ROTATIONSEN, (int)value);
        _playAudio.PlaySound(_clickSoundKey);
    }
    private void Awake()
    {
        _welcomeImage.GetComponentInChildren<TextMeshProUGUI>().DOFade(0, 1.5f)
            .SetDelay(1f)
            .OnStart(()=>
        {
            _welcomeImage.DOFade(0, 1.5f).OnComplete(() =>
          {
              _eventSystemDet.enabled = true;
              _welcomeImage.gameObject.SetActive(false);
          });
        });
        _host.onClick.AddListener(HostButton);
        _slider.value = 10;
        _playerCountText.text = _slider.value.ToString();
        _slider.onValueChanged.AddListener((float t) => SetPlayerCount((int)t));
        _setPlayerName.onClick.AddListener(SetPlayerName);
        _setRoomName.onClick.AddListener(SetRoomName);
        _keyboardMenager = this.GetComponentInChildren<KeyboardMenager>(true);
        _keyboardMenager.audioPlayer = _playAudio;
        if (!PlayerPrefs.HasKey(NICK))
        {
            PlayerPrefs.SetString(NICK, Time.realtimeSinceStartup.ToString());
        }
        if (!PlayerPrefs.HasKey(ROTATIONSEN))
        {
            PlayerPrefs.SetInt(ROTATIONSEN, 7);
        }
        _sensivitySlider.value = PlayerPrefs.GetInt(ROTATIONSEN);
        _sensivitySlider.onValueChanged.AddListener(SetRotationSen);
        _exit.onClick.AddListener(() => { Application.Quit(); });
    }
    public void SetPlayerName()
    {
        SwitchView();
        _keyboardMenager.SetTarget(NICK);
        _playAudio.PlaySound(_clickSoundKey);
    }
    public void SetRoomName()
    {
        SwitchView();
        _keyboardMenager.SetTarget(ROOMNAME);
        _playAudio.PlaySound(_clickSoundKey);
    }

    private void Start()
    {
        JoinLobby();
        _playAudio.PlayMusic(_musicSoundKey);
    }
    private void FixedUpdate()
    {
        _statusText.text = _runner.State.ToString();
        if (PlayerPrefs.HasKey(NICK) )
        {
            _playerBackNick.enabled = (PlayerPrefs.GetString(NICK) == "");
            _playerNick.text = PlayerPrefs.GetString(NICK);
        }
        if (PlayerPrefs.HasKey(ROOMNAME))
        {
            _roomBackName.enabled = (PlayerPrefs.GetString(ROOMNAME) == "");
            _roomName.text = PlayerPrefs.GetString(ROOMNAME);
        }
    }
    public async void JoinLobby()
    {
        var result = await _runner.JoinSessionLobby(SessionLobby.ClientServer);

        if (result.Ok)
        {
            // all good
        }
        else
        {
            Debug.LogError($"Failed to Start: {result.ShutdownReason}");
        }
        // This call will make Fusion join the first session as a Client

    }
    private void HostButton()
    {
        _playAudio.PlaySound(_clickSoundKey);
        if (_runner.IsShutdown)
        {
            SetButtonsOn(false);
            HostGame(1);
        }
    }
    private string _roomNameStart = "Room";
    private async void HostGame( int sceneIndex = -1)
    {
        // Create the Fusion runner and let it know that we will be providing user input
        
        // Start or join (depends on gamemode) a session with a specific name
        StartGameResult t = await _runner.StartGame(new StartGameArgs()
        {

            GameMode = GameMode.AutoHostOrClient,
            PlayerCount = _playerCount,
            SessionName = !PlayerPrefs.HasKey(ROOMNAME) ? _roomNameStart + Time.time.ToString() : PlayerPrefs.GetString(ROOMNAME),
            Scene = sceneIndex < 0 ? SceneManager.GetActiveScene().buildIndex : sceneIndex,
            SceneObjectProvider = _runner.gameObject.AddComponent<NetworkSceneManagerDefault>()
            
        }) ;
        if(!t.Ok)
        {
            SetButtonsOn();

        }
    }
    private async void JoinRoom(string text,NetworkRunner runner, int sceneIndex = -1)
    {
        _playAudio.PlaySound(_clickSoundKey);
        SetButtonsOn(false);

        StartGameResult t = await runner.StartGame(new StartGameArgs()
        {
            GameMode = GameMode.AutoHostOrClient, // Client GameMode
            SessionName = text, // Session to Join
            Scene = sceneIndex < 0 ? SceneManager.GetActiveScene().buildIndex : sceneIndex,
            SceneObjectProvider = _runner.gameObject.AddComponent<NetworkSceneManagerDefault>(), // Scene Provider
            //DisableClientSessionCreation = true, // Make sure the client will never create a Session
        });
        if (!t.Ok)
        {
            SetButtonsOn();

        }
    }

    public void SetLobbyList(NetworkRunner runner, List<SessionInfo> sessionList)
    {
        for (int i = _content.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(_content.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < sessionList.Count; i++)
        {
            if (sessionList[i].IsValid && sessionList[i].IsOpen && sessionList[i].IsVisible)
            {
                GameObject tempButton = Instantiate(_roomButtonPrefab, _content.transform);
                tempButton.GetComponent<RectTransform>().anchoredPosition =
                    Vector2.down*(tempButton.GetComponent<RectTransform>().sizeDelta.y * i);
                string t = sessionList[i].Name;
                tempButton.GetComponent<Button>().onClick.AddListener(() => {
                    _host.gameObject.SetActive(false);

                    _content.gameObject.SetActive(false); JoinRoom(t, runner,1); });
                tempButton.GetComponentInChildren<TextMeshProUGUI>().text = 
                    sessionList[i].Name + " " + sessionList[i].PlayerCount + "/" + sessionList[i].MaxPlayers;
            }
        }
    }
    private void SetButtonsOn(bool active=true)
    {
        _host.gameObject.SetActive(active);
        _content.gameObject.SetActive(active);
        if(active)
        {
            _signalBus.Subscribe<OnSessionListUpdatedSignal>(OnSessionListUpdated);
        }else
        {
            _signalBus.Unsubscribe<OnSessionListUpdatedSignal>(OnSessionListUpdated);
        }
    }
    public void OnSessionListUpdated(OnSessionListUpdatedSignal data)
    {
        SetLobbyList(data.Runner, data.SessionList);
    }
    public void SwitchView(int witchactive=0)
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).gameObject.SetActive(false);
        }
        this.transform.GetChild(witchactive).gameObject.SetActive(true);
        this.transform.GetChild(witchactive).GetComponentInChildren<Button>().Select();
    }

}
