using UnityEngine;
using Zenject;

public class LobbyInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.DeclareSignal<OnNetworkRunnerCreated>();
    }
}
public class OnNetworkRunnerCreated
{
    public OnNetworkRunnerCreated(Fusion.NetworkRunner runner)
    {
        Runner = runner;
    }
    public Fusion.NetworkRunner Runner { get; private set; }
}