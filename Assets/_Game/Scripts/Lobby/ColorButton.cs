using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ColorButton : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    [SerializeField] TMPro.TextMeshProUGUI containerText;
    [SerializeField] Image containerFillImage;
    private Button _buton;
    [SerializeField] private bool _select = false;
    public void SetContainerFillColor(Color color) => containerFillImage.color = color;
    public void SetContainerTextColor(Color color) => containerText.color = color;

    private void Start()
    {
        SetContainerFillColor(ColorDataStore.GetKeyboardFillColor());
        SetContainerTextColor(ColorDataStore.GetKeyboardTextColor());
        _buton = this.GetComponent<Button>();
        if (_select)
        {
            _buton.Select();
            ColorSelect();
        }
    }
    public void ColorSelect()
    {
        SetContainerFillColor(ColorDataStore.FillSelColor);
        SetContainerTextColor(ColorDataStore.TextSelColor);

        _select = true;
    }
    public void ColorDeSelect()
    {
        SetContainerFillColor(ColorDataStore.GetKeyboardFillColor());
        SetContainerTextColor(ColorDataStore.GetKeyboardTextColor());

        _select = false;
    }
    public void OnSelect(BaseEventData eventData = null)
    {
        ColorSelect();
    }

    public void OnDeselect(BaseEventData eventData = null)
    {
        ColorDeSelect();
    }
    private void OnDisable()
    {
        ColorDeSelect();
    }
}
