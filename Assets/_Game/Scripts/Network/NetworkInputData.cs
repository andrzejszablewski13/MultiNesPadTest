using Fusion;
using UnityEngine;

public struct NetworkInputData : INetworkInput
{
    public const byte ATTACK = 1;
    public const byte SPECIAL = 2;
    public const byte SELECT = 4;
    public const byte START = 8;
    public const byte RESPAWN = 16;
    public byte buttons;
    public Vector3 direction;
}