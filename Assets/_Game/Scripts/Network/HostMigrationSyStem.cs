﻿using Fusion;
using UnityEngine;
public partial class PhotonConnectionSyStem
{
    public class HostMigrationSyStem
    {
        public async void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken, RunnerNetworkFactory.Factory _runnerPrefab)
        {

            // Step 3.
            // Shutdown the current Runner, this will not be used anymore. Perform any prior setup and tear down of the old Runner

            // The new "ShutdownReason.HostMigration" can be used here to inform why it's being shut down in the "OnShutdown" callback
            await runner.Shutdown(shutdownReason: ShutdownReason.HostMigration);

            // Step 4.
            // Create a new Runner.
            var newRunner = _runnerPrefab.Create().GetComponent<NetworkRunner>();
            newRunner.transform.parent = null;
            newRunner.ProvideInput = true;
            newRunner.AddCallbacks(_instance);

            // setup the new runner...

            // Start the new Runner using the "HostMigrationToken" and pass a callback ref in "HostMigrationResume".
            StartGameResult result = await newRunner.StartGame(new StartGameArgs()
            {
                // SessionName = SessionName,              // ignored, peer never disconnects from the Photon Cloud
                // GameMode = gameMode,                    // ignored, Game Mode comes with the HostMigrationToken
                HostMigrationToken = hostMigrationToken,   // contains all necessary info to restart the Runner
                HostMigrationResume = HostMigrationResume, // this will be invoked to resume the simulation
                SceneObjectProvider = newRunner.gameObject.AddComponent<NetworkSceneManagerDefault>()                                           // other args
            });

            // Check StartGameResult as usual
            if (result.Ok == false)
            {
                Debug.LogWarning(result.ShutdownReason);
            }
            else
            {
                Debug.Log("Done");
            }
        }
        void HostMigrationResume(NetworkRunner runner)
        {
            Debug.Log("test");
            // Get a temporary reference for each NO from the old Host
            foreach (var resumeNO in runner.GetResumeSnapshotNetworkObjects())
            {
                Debug.Log(resumeNO.Name);
                if (
                        // Extract any NetworkBehavior used to represent the position/rotation of the NetworkObject
                        // this can be either a NetworkTransform or a NetworkRigidBody, for example
                        resumeNO.TryGetBehaviour<NetworkPositionRotation>(out var posRot))
                {

                    runner.Spawn(resumeNO, position: posRot.ReadPosition(), rotation: posRot.ReadRotation(), onBeforeSpawned: (runner, newNO) =>
                    {
                        // One key aspects of the Host Migration is to have a simple way of restoring the old NetworkObjects state
                        // If all state of the old NetworkObject is all what is necessary, just call the NetworkObject.CopyStateFrom
                        newNO.CopyStateFrom(resumeNO);

                        // and/or

                        // If only partial State is necessary, it is possible to copy it only from specific NetworkBehaviours
                        if (resumeNO.TryGetBehaviour<NetworkBehaviour>(out var myCustomNetworkBehaviour))
                        {
                            newNO.GetComponent<NetworkBehaviour>().CopyStateFrom(myCustomNetworkBehaviour);
                        }
                    });
                }
            }
        }
    }
}


