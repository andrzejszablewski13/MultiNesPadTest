using Fusion;
using Fusion.Sockets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Zenject;
using System.Threading.Tasks;
public partial class PhotonConnectionSyStem : MonoBehaviour, INetworkRunnerCallbacks
{

    [SerializeField] private bool _debugModeOn = true;

    private SignalBus _signalBus=null;
    private HostMigrationSyStem _migrationSystem;
    private RunnerNetworkFactory.Factory _factory;

    [Inject]
    private void InjectData(SignalBus bus, RunnerNetworkFactory.Factory factory)
    {
        _signalBus = bus;
        _factory = factory;
    }
    TaskCompletionSource<bool> IsSomethingLoading = new TaskCompletionSource<bool>();
    public async void OnPlayerJoined(NetworkRunner runner, PlayerRef player)
    {
        await IsSomethingLoading.Task;
        _signalBus.Fire<OnPlayerJoinSignal>(new OnPlayerJoinSignal(runner, player));
        if (_debugModeOn)
        {
            Debug.Log(_signalBus.IsSignalDeclared(typeof(OnPlayerJoinSignal)) + " " + _signalBus.NumSubscribers);

            Debug.Log("PlayerJoin");
        }
        //if hosting order is: 1.StartLoadScene 2.ZenjectBinding 3.EndLoadScene 4.PlayerJoined
        //if shared order is player join first then rest (even scene change)
    }

    public void OnPlayerLeft(NetworkRunner runner, PlayerRef player)
    {
        _signalBus.Fire<OnPlayerLeftSignal>(new OnPlayerLeftSignal(runner, player));
        if (_debugModeOn)
            Debug.Log("PlayerLeft");
    }
    public void OnInput(NetworkRunner runner, NetworkInput input)
    {
        _signalBus.Fire<OnPlayerInputSignal>(new OnPlayerInputSignal(runner, input));
    }
    public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input) { }
    public void OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason) {
        if (_debugModeOn)
            Debug.Log("Shutdown "+ shutdownReason); 
    }
    public void OnConnectedToServer(NetworkRunner runner) { if (_debugModeOn) Debug.Log("Connected"); }
    public void OnDisconnectedFromServer(NetworkRunner runner) { if (_debugModeOn) Debug.Log("Disconected"); }
    public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token) { }
    public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason) { }
    public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message) { }
    public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
    {
        if (_debugModeOn) Debug.Log("SessionListUpdate");
        _signalBus.Fire<OnSessionListUpdatedSignal>(new OnSessionListUpdatedSignal(runner, sessionList));

    }
    public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data) { }


    public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken){
        _migrationSystem.OnHostMigration(runner, hostMigrationToken, _factory);
    }
    
    public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ArraySegment<byte> data) { }
    public void OnSceneLoadDone(NetworkRunner runner) 
    {
        if (_debugModeOn) Debug.Log("EndLoad");
        IsSomethingLoading.SetResult(true);
    }
    public void OnSceneLoadStart(NetworkRunner runner) {
        if (_debugModeOn) Debug.Log("StartLoad");
    }
    private static PhotonConnectionSyStem _instance; 
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        if (_instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
        Application.quitting += () => { _runner.Shutdown(); };
        _runner = _factory.Create().GetComponent<NetworkRunner>();
        _runner.ProvideInput = true;
        _runner.transform.parent = null;
        _runner.AddCallbacks(_instance);
        _signalBus.TryFire<OnNetworkRunnerCreated>(new OnNetworkRunnerCreated(_runner));
        _migrationSystem = new HostMigrationSyStem();
    }
    private NetworkRunner _runner;
    public class Factory:PlaceholderFactory<PhotonConnectionSyStem>
    {

    }
}


