using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;
public class InputHandler : MonoBehaviour
{
    private SignalBus _signalBus = null;
    private InputMap _inputMap;
    private GameCanvasMenager _ui;

    [Inject]
    private void InjectData(SignalBus bus, InputMap inputMap, GameCanvasMenager ui)
    {
        _ui = ui;
        _ui.Respawn.onClick.AddListener(() => _respawn = true);
        _inputMap = inputMap;
        _signalBus = bus;
        _signalBus.Subscribe<OnPlayerInputSignal>(OldInputHandler);
        _inputMap.Legal.Enable();
        _inputMap.Legal.Move.performed += Move;
        _inputMap.Legal.Move.canceled += Move;
        _inputMap.Legal.Attack.performed += Attack;
        _inputMap.Legal.Attack.canceled += Attack;
        _inputMap.Legal.Special.performed += Special;
        _inputMap.Legal.Special.canceled += Special;
        _inputMap.Legal.Select.performed += Select;
        _inputMap.Legal.Select.canceled += Select;
        _inputMap.Legal.Start.performed += StartButton;
        _inputMap.Legal.Start.canceled += StartButton;
    }
    private Vector3 _direction;
    private float _smothingvalue = 0.2f;
    private void Move(InputAction.CallbackContext obj)
    {

        _direction = new Vector3(obj.ReadValue<Vector2>().x, 0, obj.ReadValue<Vector2>().y);
    }

    private bool _attack=false, _special = false, _select = false, _start = false,_respawn=false;
    private void Attack(InputAction.CallbackContext obj)
    {
        _attack = obj.performed;
    }
    private void Special(InputAction.CallbackContext obj)
    {
        _special = obj.performed;
    }
    private void Select(InputAction.CallbackContext obj)
    {
        _select = obj.performed;
    }
    private void StartButton(InputAction.CallbackContext obj)
    {
        _start = obj.performed;
    }
    private Vector3 _dir;

    private void OldInputHandler(OnPlayerInputSignal data)
    {
        
        if (_direction == Vector3.zero)
        {
            _dir = _direction;
        }else
        {
            _dir = Vector3.Lerp(_dir, _direction, _smothingvalue);
            Vector3.ClampMagnitude(_dir, 1f);
        }
        var stru = new NetworkInputData
        {
            direction = _dir
        };

        if (_attack)
            stru.buttons |= NetworkInputData.ATTACK;

        if (_special)
            stru.buttons |= NetworkInputData.SPECIAL;
        if (_select)
            stru.buttons |= NetworkInputData.SELECT;
        _select = false;

        if (_start)
            stru.buttons |= NetworkInputData.START;
        _start = false;
        if (_respawn)
            stru.buttons |= NetworkInputData.RESPAWN;
        _respawn = false;


        data.Input.Set(stru);
    }
    private void OnDestroy()
    {
        _signalBus.Unsubscribe<OnPlayerInputSignal>(OldInputHandler);
        _inputMap.Legal.Disable();
        _inputMap.Legal.Move.performed -= Move;
        _inputMap.Legal.Move.canceled -= Move;
        _inputMap.Legal.Attack.performed -= Attack;
        _inputMap.Legal.Attack.canceled -= Attack;
        _inputMap.Legal.Special.performed -= Special;
        _inputMap.Legal.Special.canceled -= Special;
        _inputMap.Legal.Select.performed -= Select;
        _inputMap.Legal.Select.canceled -= Select;
        _inputMap.Legal.Start.performed -= StartButton;
        _inputMap.Legal.Start.canceled -= StartButton;
    }


}
